import java.io.IOException;
import java.util.Scanner;

public class App {
    public static void main(String[] args) throws IOException {
        Scanner kb = new Scanner(System.in);
        String input, output;
        while (true) {
            System.out.print("Enter infix: ");
            System.out.flush();
            input = kb.next();
            if (input.equals("")) {
                break;
            }
            InToPost theTrans = new InToPost(input);
            output = theTrans.doTrans();
            System.out.println("Postfix is " + output + '\n');
        }
    }

    
}
